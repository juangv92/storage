import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {ProductsComponent} from './products/products.component';

@Injectable()
export class ProductFirstGuard {
  private firstNavigation = true;

  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.firstNavigation) {
      this.firstNavigation = false;
      if (route.component !== ProductsComponent) {
        this.router.navigateByUrl('/');
        return false;
      }
    }
    return true;
  }
}
