import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {RouterModule, Routes} from '@angular/router';
import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import {ProductRepository} from './products/product.repository';
import {StaticDataSource} from './static.data.source';
import { HeaderComponent } from './header/header.component';
import { NavComponent } from './nav/nav.component';
import { TracesComponent } from './traces/traces.component';
import { FooterComponent } from './footer/footer.component';
import { ProductEditorComponent } from './product-editor/product-editor.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductFirstGuard } from './product.first.guard';
import {MaterialModule} from './material/material.module';
import {MatSelectModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatRadioModule} from '@angular/material';
import {MatBadgeModule} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    HeaderComponent,
    NavComponent,
    TracesComponent,
    FooterComponent,
    ProductEditorComponent,
  ],
  imports: [
    MaterialModule,
    BrowserAnimationsModule,
    MatSelectModule,
    BrowserModule,
    FormsModule,
    MatRadioModule,
    MatBadgeModule,
    ReactiveFormsModule,
    AngularFontAwesomeModule,
    RouterModule.forRoot([
      {path: 'product', component: ProductsComponent, canActivate: [ProductFirstGuard]},
      {path: 'editor', component: ProductEditorComponent, canActivate: [ProductFirstGuard]},
      {path: 'product/:mode/:type/:code', component: ProductEditorComponent, canActivate: [ProductFirstGuard]},
      {path: '**', redirectTo: '/product'}
    ])
  ],
  providers: [ProductRepository, StaticDataSource, ProductFirstGuard],
  bootstrap: [HeaderComponent, NavComponent, AppComponent, TracesComponent, FooterComponent]
})
export class AppModule { }
