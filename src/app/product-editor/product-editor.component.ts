import {Component} from '@angular/core';
import {Product, SimpleProduct, ComplexProduct, ProductIngredient} from '../products/product';
import {ProductRepository} from '../products/product.repository';
import {NgForm} from '@angular/forms';
import {StaticDataSource} from '../static.data.source';
import {ActivatedRoute} from '@angular/router';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-product-editor',
  templateUrl: './product-editor.component.html',
  styleUrls: ['./product-editor.component.css']
})
export class ProductEditorComponent {
  public product = new Product();
  public simpleProduct = new SimpleProduct();
  public complexProduct = new ComplexProduct();
  public ingredientProduct = new ProductIngredient();
  public ingredients: ProductIngredient[] = [];
  public price: number;
  productType = 'SimpleProduct';
  editing = false;
  code: string;
  codeGenerate: string;
  selected: string = null;
  haveQuantity = true;

  constructor(private dataSource: StaticDataSource, private repository: ProductRepository, activeRoute: ActivatedRoute) {
    this.editing = activeRoute.snapshot.params['mode'] === 'editing';
    if (this.editing) {
      this.code = activeRoute.snapshot.params['code'];
      this.productType = activeRoute.snapshot.params['type'];
      Object.assign(this.product, repository.getProduct(activeRoute.snapshot.params['code']));
    }
    this.codeGenerate = this.repository.generateCode();
  }

  save(form: NgForm) {
    if (form.valid) {
      if (this.productType === 'SimpleProduct') {
        // Create a new SimpleProduct taking values from ngForm
        this.simpleProduct = new SimpleProduct(this.codeGenerate, this.product.storage_id,
          this.product.unit_id, this.product.name, this.product.description, this.product.quantity,
          this.product.prefered, this.simpleProduct.cost_price);
        console.log(this.simpleProduct.cost_price);
        this.dataSource.saveProduct(this.simpleProduct, this.productType, this.editing, this.code);
      } else {
        // Create a new ComplexProduct taking values from ngForm
        this.complexProduct = new ComplexProduct(this.codeGenerate, this.product.storage_id,
          this.product.unit_id, this.product.name, this.product.description, this.product.quantity,
          this.product.prefered);
        this.dataSource.saveProduct(this.complexProduct, this.productType, this.editing, this.code, this.ingredients);
      }
    }
  }

  addIngredient(form: NgForm) {
    if (form.valid) {
      this.ingredients.push(
        new ProductIngredient(this.codeGenerate, this.selected, this.ingredientProduct.quantity * this.product.quantity)
      );
    }
    console.log(this.codeGenerate, this.selected, this.ingredientProduct.quantity);
  }

  getIngredients() {
    return this.ingredients;
  }

  public resetTypeProduct(): string {
    return this.productType = 'SimpleProduct';
  }

  public getProducts(): Product[] {
    return this.repository.getAllProducts();
  }

  public getProduct(code: string): Product {
    return this.repository.getProduct(code);
  }

  public getNameByCode(code: string) {
    return this.repository.getNameByCode(code);
  }

  deleteIngredient(code: string) {
    this.ingredients.splice(this.ingredients.findIndex(p => p.ingredient_code === code), 1);
  }

  checkedQuantity(): boolean {
    return this.repository.checkedQuantity(this.selected, this.ingredientProduct.quantity * this.product.quantity);
  }

  isIngredient(product: Product): boolean {
    for (const ingredient of this.ingredients) {
      if (ingredient.ingredient_code === product.code) {
        return true;
      }
    }
    return false;
  }

  getPrice(product) {
    const prod = product as SimpleProduct;
    return prod.getPrice();
  }

}
