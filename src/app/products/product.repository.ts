import {Injectable} from '@angular/core';
import {ComplexProduct, Product, ProductIngredient, SimpleProduct} from './product';
import {StaticDataSource} from '../static.data.source';
import {ProductEditorComponent} from '../product-editor/product-editor.component';
import {forEach} from '@angular/router/src/utils/collection';

@Injectable()
export class ProductRepository {
  products: Product[];
  product = new Product();
  ingredients: ProductIngredient[] = [];

  constructor(private dataSource: StaticDataSource) {
    dataSource.getProducts().subscribe(data => {
      this.products = data;
    });
  }

  getProduct(code: string): Product {
    return this.products.find(p => p.code === code);
  }

  public getNameByCode(code: string) {
    return this.products.find(p => p.code === code).name;
  }


  deleteProduct(code: string) {
    this.products.splice(this.products.findIndex(p => p.code === code), 1);
  }

  getAllProducts(): Product[] {
    return this.products;
  }

  getProducts(typeProduct?: string): Product[] {
    return this.products.filter(p => {
      if (typeProduct === 'SimpleProduct' && p instanceof SimpleProduct) {
        return p;
      } else if (typeProduct === 'ComplexProduct' && p instanceof ComplexProduct) {
        return p;
      }
    });
  }

  generateCode() {
    let text = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    for (let i = 0; i < 5; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  getIngredientsToString(code: string): string {
    let text = '';
    for (const ingredient of this.getAllProducts()) {
      if (ingredient instanceof ProductIngredient) {
        const ing = ingredient as ProductIngredient;
        if (ing.origin_product_code === code) {
          text += this.getNameByCode(ing.ingredient_code) + '-(' + ing.quantity + ')   ';
        }
      }
    }
    return text;
  }

  getQuantity(product: Product): number {
    let quantity = product.quantity;
    for (const ingredient of this.getAllProducts()) {
      if (ingredient instanceof ProductIngredient) {
        const ing = ingredient as ProductIngredient;
        if (ing.ingredient_code === product.code) {
          quantity = quantity - ing.quantity;
        }
      }
    }
    return quantity;
  }

  checkedQuantity(code: string, quantity: number): boolean {
    const product = this.getProduct(code);
    for (const prod of this.getAllProducts()) {
      if (prod.code === product.code && prod.quantity >= quantity) {
        console.log(prod.quantity);
        return true;
      }
    }
    return false;
  }

  getProductsIngredients(product: Product): ProductIngredient[] {
    const ingredients = new Array<ProductIngredient>();
    for (const ingredient of this.getAllProducts()) {
      if (ingredient instanceof ProductIngredient) {
        const ing = ingredient as ProductIngredient;
        if (ing.origin_product_code === product.code) {
          ingredients.push(ing);
        }
      }
    }
    return ingredients;
  }

}
