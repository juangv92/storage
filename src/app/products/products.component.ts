import {Component, OnInit} from '@angular/core';
import {ComplexProduct, Product, ProductIngredient, SimpleProduct} from './product';
import {ProductRepository} from './product.repository';
import {Router} from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  public selectedTypeProduct = 'SimpleProduct';
  public productsPerPage = 3;
  public selectedPage = 1;
  codeGenerate: string;

  constructor(private repository: ProductRepository, private router: Router) {
  }

  getProductsComponent(): Product[] {
    const pageIndex = (this.selectedPage - 1) * this.productsPerPage;
    return this.repository.getProducts(this.selectedTypeProduct)
      .slice(pageIndex, pageIndex + this.productsPerPage);
  }

  public changeTypeProduct(category: string) {
    this.selectedTypeProduct = category;
  }

  public getTypeProduct() {
    return this.selectedTypeProduct;
  }

  public addProduct() {
    this.router.navigateByUrl('/editor');
  }

  public deleteProduct(code: string) {
    this.repository.deleteProduct(code);
  }

  public changePage(newPage: number) {
    this.selectedPage = newPage;
  }

  public changePageSize(newSize: number) {
    this.productsPerPage = Number(newSize);
    this.changePage(1);
  }

  public getPageNumbers(): number[] {
    return Array(
      Math.ceil(this.repository.getProducts(this.selectedTypeProduct).length / this.productsPerPage)).fill(0).map((x, i) => i + 1);
  }

  public resetPage() {
    this.selectedPage = 1;
  }

  generateCode() {
    return this.codeGenerate = this.repository.generateCode();
  }

  getIngredientsToString(code: string) {
    return this.repository.getIngredientsToString(code);
  }

  getQuantity(product: Product): number {
    return this.repository.getQuantity(product);
  }

  getPrice(product: Product): number {
    if (this.selectedTypeProduct === 'SimpleProduct') {
      return this.getProductsPrice(product.code);
    } else {
      let price = 0;
      for (const ingredient of this.repository.getProductsIngredients(product)) {
        const partialPrice = ingredient.quantity * this.getProductsPrice(ingredient.ingredient_code);
        price = price + (partialPrice + (partialPrice * 0.20));
      }
      return price;
    }
  }

  getProductsPrice(code: string): number {
    const product = this.repository.getProduct(code) as SimpleProduct;
    console.log(product.cost_price);
    return product.cost_price;
  }

  ngOnInit() {
  }

}
