export class Product {
  constructor(public code?: string,
              public storage_id?: number,
              public unit_id?: number,
              public name?: string,
              public description?: string,
              public quantity?: number,
              public prefered?: boolean) {
  }
}

export class ProductIngredient {
  constructor(public origin_product_code?: string,
              public ingredient_code?: string,
              public quantity?: number) {
  }
}

export class SimpleProduct extends Product {
  product_code?: string;

  constructor(public code?: string,
              public storage_id?: number,
              public unit_id?: number,
              public name?: string,
              public description?: string,
              public quantity?: number,
              public prefered?: boolean,
              public cost_price?: number) {
    super(code, storage_id, unit_id, name, description, quantity, prefered);
    this.product_code = code;
  }

  public getPrice() {
    return this.cost_price;
  }
}

export class ComplexProduct extends Product {
  product_code: string;

  constructor(public code?: string,
              public storage_id?: number,
              public unit_id?: number,
              public name?: string,
              public description?: string,
              public quantity?: number,
              public prefered?: boolean) {
    super(code, storage_id, unit_id, name, description, quantity, prefered);
    this.product_code = code;
  }
}
