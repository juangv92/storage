import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/from';
import {ComplexProduct, Product, SimpleProduct, ProductIngredient} from './products/product';
import {Unit} from './unit';
import {Route, Router} from '@angular/router';

@Injectable()

export class StaticDataSource {
  public products: Product[];
  public units: Unit[];

  constructor(private router: Router) {
    this.products = new Array<Product>(
      new SimpleProduct('CODE', 1, 1, 'Oil', 'A grass for voild food', 275, true, 120),
      new SimpleProduct('CODE1', 1, 1, 'Salt', 'One person A boat for', 100, true, 58),
      new ComplexProduct('CODE2', 1, 1, 'Meet', 'FOOD for people', 1, true),
      new SimpleProduct('CODE3', 1, 1, 'Milk', 'I dont Not', 19, true, 49),
      new SimpleProduct('CODE4', 1, 1, 'Apple', 'All Mett for to', 15, true, 100),
      new ComplexProduct('CODE5', 1, 1, 'More Meet', 'FOOD for people', 1, true),
      new ProductIngredient('CODE2', 'CODE', 5),
      new ProductIngredient('CODE5', 'CODE', 3),
      new ProductIngredient('CODE5', 'CODE4', 3)
    );
    this.units = new Array<Unit>(
      new Unit(1, 'kg'),
      new Unit(2, 'l'),
      new Unit(3, 'g'),
      new Unit(4, 'u')
    );
  }


  public getProducts() {
    return Observable.from([this.products]);
  }



  saveProduct(product: Product, productType: string, editing: boolean, code: string, ingredients?: ProductIngredient[]) {
    Observable.from([product]).subscribe(p => {
      if (productType === 'SimpleProduct') {
        const temp = p as SimpleProduct;
        console.log(temp.cost_price);
        if (editing) {
          this.products.splice(this.products.findIndex(o => o.code === code), 1);
        }
        this.products.push(
          new SimpleProduct(p.code, p.storage_id, p.unit_id, p.name, p.description, p.quantity, p.prefered, temp.cost_price));
      } else {
        const temp = p as ComplexProduct;
        if (editing) {
          this.products.splice(this.products.findIndex(o => o.code === code), 1);
        }
        const complex = new ComplexProduct(p.code, p.storage_id, p.unit_id, p.name, p.description, p.quantity, p.prefered);
        this.products.push(complex);

        let ing;

        for (const ingredient of ingredients) {
          ing = new ProductIngredient(ingredient.origin_product_code, ingredient.ingredient_code, ingredient.quantity);
          this.products.push(ing);
        }
      }

    });
    this.router.navigateByUrl('/product');
  }
}
